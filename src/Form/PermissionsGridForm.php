<?php

namespace Drupal\permission_grid\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\user\PermissionHandlerInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for a single role's permissions grid.
 */
class PermissionsGridForm extends FormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Permission handler service.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Creates a PermissionsGridForm instance.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The Permission handler service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(
    ModuleHandlerInterface $module_handler,
    PermissionHandlerInterface $permission_handler,
    MessengerInterface $messenger
  ) {
    $this->moduleHandler = $module_handler;
    $this->permissionHandler = $permission_handler;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('user.permissions'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'permissions_grid_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, RoleInterface $user_role = NULL) {
    // We have to load this explicitly because it won't match any module.
    $this->moduleHandler->loadInclude('permission_grid', 'inc', 'permission_grid.permissions');

    $form['#title'] = $this->t('Permissions grid for %role', [
      '%role' => $user_role->label(),
    ]);

    // Invoke hook_permission_grid_info().
    $permission_info = $this->moduleHandler->invokeAll('permission_grid_info');
    $this->moduleHandler->alter('permission_grid_info', $permission_info);

    // Fetch permissions for the one selected role.
    // Gets an array keyed by rid, with an array of permissions.
    $role_permissions = user_role_permissions([$user_role->id()])[$user_role->id()];

    // One table for each object type, e.g. node types, vocabularies, etc.
    foreach ($permission_info as $object_type => $object_permission_info) {
      $form['permissions'][$object_type] = [
        '#type' => 'table',
        '#header' => [$this->t('Permission')],
        '#caption' => Html::escape($object_permission_info['label']),
        '#rows' => [],
      ];

      // One table row for each object, e.g. each node type.
      foreach ($object_permission_info['objects'] as $object => $object_label) {
        $verbs = [];
        // Empty header cell above the object name.
        $verbs[] = '';

        // One column for each verb, e.g. 'create', 'edit'.
        foreach ($object_permission_info['verb_groups'] as $verb_group) {
          // Collate verbs for the header.
          $verbs += $verb_group['verbs'];

          foreach ($verb_group['verbs'] as $verb => $verb_label) {
            $permission_string = str_replace(
              array('%verb', '%object'),
              array($verb, $object),
              $verb_group['pattern']
            );

            // Create a cell for the object label.
            $form['permissions'][$object_type][$object]['_label'] = [
              '#plain_text' => $object_label,
              '#wrapper_attributes' => [
                'header' => TRUE,
              ],
            ];

            // Create a checkbox for the permission.
            $form['permissions'][$object_type][$object][$verb] = [
              '#type' => 'checkbox',
              '#default_value' => in_array($permission_string, $role_permissions),
              // Set this manually because of http://drupal.org/node/1130946. TODO?
              '#parents' => array('permission', $permission_string),
              '#title' => $permission_string,
              '#title_display' => 'invisible',
            ];
          }
        }

        $form['permissions'][$object_type]['#header'] = $verbs;
      }
    }

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save permissions'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user_role = $form_state->getBuildInfo()['args'][0];
    user_role_change_permissions($user_role->id(), $form_state->getValue('permission'));

    $this->messenger->addStatus($this->t('The changes have been saved.'));
  }

}
