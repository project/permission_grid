<?php

namespace Drupal\permission_grid\Controller;

use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\RoleInterface;

/**
 * Shows a list of roles with links to edit their permissions grid.
 */
class PermissionsGridRolesOverviewController {

  use StringTranslationTrait;

  /**
   * Callback for the permission_grid.roles_overview route.
   */
  public function content() {
    $header = [
      $this->t('Name'),
      [
        'data' => $this->t('Operations'),
        'colspan' => 2,
      ],
    ];

    foreach (user_roles() as $rid => $role) {
      $edit_permissions = Link::createFromRoute($this->t('edit permissions grid'), 'permission_grid.role', [
        'user_role' => $rid,
      ]);
      switch ($rid) {
        case RoleInterface::ANONYMOUS_ID:
        case RoleInterface::AUTHENTICATED_ID:
          $rows[] = [
            $role->label(),
            $this->t('locked'),
            $edit_permissions,
          ];
          break;
        default:
          $rows[] = [
            $role->label(),
            Link::createFromRoute($this->t('edit role'), 'entity.user_role.edit_form', [
              'user_role' => $rid,
            ]),
            $edit_permissions,
          ];
      }
    }

    $build['roles_list'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }

}
