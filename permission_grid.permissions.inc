<?php
/**
 * @file permission_grid.permissions.inc
 * Contains hook implementations on behalf of other modules.
 */

// =========================================================== Core modules

/**
 * Implements hook_permission_grid_info() on behalf of node module.
 */
function node_permission_grid_info() {
  $return = [
    'node' => [
      'label' => t('Content type'),
      'objects' => [],
      'verb_groups' => [
        'core' => [
          'pattern' => '%verb %object content',
          'verbs' => [
            'create'      => t('Create'),
            'edit own'    => t('Edit own'),
            'edit any'    => t('Edit any'),
            'delete own'  => t('Delete own'),
            'delete any'  => t('Delete any'),
          ],
        ],
        'revisions' => [
          'pattern' => '%verb %object revisions',
          'verbs' => [
            'view'        => t('View revisions'),
            'revert'      => t('Revert revisions'),
            'delete'      => t('Delete revisions'),
          ],
        ],
      ],
    ],
  ];

  $node_types = \Drupal::service('entity_type.manager')->getStorage('node_type')->loadMultiple();
  foreach ($node_types as $id => $type) {
    $return['node']['objects'][$id] = $type->label();
  }

  return $return;
}

/**
 * Implements hook_permission_grid_info() on behalf of media module.
 */
function media_permission_grid_info() {
  $return = [
    'media' => [
      'label' => t('Media type'),
      'objects' => [],
      'verb_groups' => [
        'core' => [
          'pattern' => '%verb %object media',
          'verbs' => [
            'create'      => t('Create'),
            'edit own'    => t('Edit own'),
            'edit any'    => t('Edit any'),
            'delete own'  => t('Delete own'),
            'delete any'  => t('Delete any'),
          ],
        ],
      ],
    ],
  ];

  $media_types = \Drupal::service('entity_type.manager')->getStorage('media_type')->loadMultiple();
  foreach ($media_types as $id => $type) {
    $return['media']['objects'][$id] = $type->label();
  }

  return $return;
}

/**
 * Implements hook_permission_grid_info() on behalf of taxonomy module.
 */
function taxonomy_permission_grid_info() {
  $vocabularies = \Drupal::service('entity_type.manager')->getStorage('taxonomy_vocabulary')->loadMultiple();

  if (empty($vocabularies)) {
    return [];
  }

  $return = array(
    'vocabulary' => array(
      'label' => t('Taxonomy'),
      'objects' => array(),
      'verb_groups' => array(
        'core' => array(
          'pattern' => '%verb terms in %object',
          'verbs' => array(
            'edit'    => t('Edit terms'),
            'delete'  => t('Delete terms'),
          ),
        )
      ),
    ),
  );

  foreach ($vocabularies as $vocabulary) {
    $return['vocabulary']['objects'][$vocabulary->id()] = $vocabulary->label();
  }

  return $return;
}

// =========================================================== Contrib modules

/**
 * Implements hook_permission_grid_info_alter() on behalf of vppr module.
 */
function Xvppr_permission_grid_info_alter(&$info) {
  $info['vocabulary']['verb_groups']['vppr'] = array(
    'pattern' => '%verb %object vocabulary terms',
    'verbs' => array(
      'administer' => t('Administer terms'),
    ),
  );
}

/**
 * Implements hook_permission_grid_info() on behalf of profile2 module.
 */
function Xprofile2_permission_grid_info() {
  $return = array(
    'profile2' => array(
      'label' => t('Profiles'),
      'objects' => array(),
      'verb_groups' => array(
        'core' => array(
          'pattern' => '%verb %object profile',
          'verbs' => array(
            'edit own'  => t('Edit own'),
            'edit any'  => t('Edit any'),
            'view own'  => t('View own'),
            'view any'  => t('View any'),
          ),
        ),
      ),
    ),
  );

  foreach (profile2_get_types() as $type => $type_info) {
    $return['profile2']['objects'][$type] = $type_info->label;
  }

  return $return;
}

/**
 * Implements hook_permission_grid_info() on behalf of commerce module.
 */
function Xcommerce_permission_grid_info() {
  $info = array();

  // @todo: other commerce entity types.
  foreach (array('commerce_product') as $entity_type) {
    $entity_info = entity_get_info($entity_type);
    $types = array();
    foreach ($entity_info['bundles'] as $bundle => $bundle_info) {
      $types[$bundle] = $bundle_info['label'];
    }

    $info[$entity_type] = array(
      'label' => $entity_info['label'],
      'objects' => $types,
      'verb_groups' => array(
        // Put plural first so 'create' comes first.
        'commerce_plural' => array(
          'pattern' => "%verb $entity_type entities of bundle %object",
          'verbs' => array(
            'create' => t('Create'),
            'edit own' => t('Edit own'),
            'view own' => t('View own'),
          ),
        ),
        'commerce_singular' => array(
          'pattern' => "%verb $entity_type entity of bundle %object",
          'verbs' => array(
            'edit any' => t('Edit any'),
            'view any' => t('View any'),
          ),
        ),
      ),
    );
  }
  return $info;
}

/**
 * Implements hook_permission_grid_info() on behalf of flag module.
 */
function Xflag_permission_grid_info() {
  $return = array(
    'flag' => array(
      'label' => t('Flag'),
      'objects' => array(),
      'verb_groups' => array(
        'flag' => array(
          'pattern' => '%verb %object',
          'verbs' => array(
            'flag'      => t('Flag'),
            'unflag'    => t('Unflag'),
          ),
        ),
      ),
    ),
  );

  foreach (flag_get_flags() as $flag) {
    $return['flag']['objects'][$flag->name] = $flag->title;
  }

  return $return;
}

/**
 * Implements hook_permission_grid_info() on behalf of eck module.
 */
function Xeck_permission_grid_info() {
  module_load_include('inc', 'eck', 'eck.entity_type');
  module_load_include('inc', 'eck', 'eck.bundle');

  foreach (EntityType::loadAll() as $entity_type) {
    $return[$entity_type->name] = array(
      'label' => $entity_type->label,
      'verb_groups' => array(
        'eck' => array(
          'pattern' => "eck %verb $entity_type->name %object entities",
          'verbs' => array(
            'add'      => t('Add'),
            'edit'    => t('Edit'),
            'delete'  => t('Delete'),
            'list'    => t('View list of'),
            'view'    => t('View'),
          ),
        ),
      ),
    );

    foreach (Bundle::loadByEntityType($entity_type) as $bundle) {
      $return[$entity_type->name]['objects'][$bundle->name] = $bundle->label;
    }
  }

  return $return;
}
